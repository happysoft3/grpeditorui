
#include "pixelbuffer.h"
#include <stdexcept>

PixelBuffer::PixelBuffer(uint32_t buffsize)
{
    m_pixelwidth = 0;
    m_pixelheight = 0;
    if (buffsize != 0)
        m_pixelstream.reserve(buffsize);
}

PixelBuffer::~PixelBuffer()
{ }

bool PixelBuffer::GetPixel(uint32_t &dest, const size_t &index)
{
    try
    {
        dest = m_pixelstream.at(index);
    }
    catch (const std::out_of_range &)
    {
        return false;
    }
    return true;
}