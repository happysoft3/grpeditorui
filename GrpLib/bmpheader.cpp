
#include "bmpheader.h"
#include "bmpFormat.h"

struct BmpHeader::BmpBase
{
    uint16_t m_magic;
    uint32_t m_filesize;
    uint16_t m_commitLow;
    uint16_t m_commitHigh;
    uint32_t m_pixelOff;
};

struct BmpHeader::BmpDib
{
    uint32_t m_dibHdSize;
    uint32_t m_bmpWidth;
    uint32_t m_bmpHeight;
    uint16_t m_colorPlane;
    uint16_t m_bits;
    uint32_t m_compressType;
    uint32_t m_pixelSize;
    uint32_t m_screenX;
    uint32_t m_screenY;
    uint32_t m_paletcol;
    uint32_t m_tpyIgnore;
};

BmpHeader::BmpHeader(uint32_t ttsize)
    : m_bmpbase(new BmpBase({ })), m_bmpdib(new BmpDib({ }))
{
    m_bmpbase->m_magic = 0x4d42;
    m_bmpbase->m_pixelOff = sizeof(BmpBase) + sizeof(BmpDib);
    m_bmpdib->m_dibHdSize = sizeof(BmpDib);
    m_bmpdib->m_colorPlane = 1;
    m_bmpdib->m_bits = 24;
    SetFilesize(ttsize);

    m_ctx = nullptr;
    m_writeIndex = 0;
}

BmpHeader::~BmpHeader()
{ }

void BmpHeader::SetFilesize(uint32_t ttsize)
{
    m_bmpbase->m_filesize = ttsize;
    m_bmpdib->m_pixelSize = ttsize - m_bmpbase->m_pixelOff;
}

void BmpHeader::SetPixelSize(uint32_t width, uint32_t height)
{
    m_bmpdib->m_bmpWidth = width;
    m_bmpdib->m_bmpHeight = height;
}

uint32_t BmpHeader::TotalFieldLength()
{
    return sizeof(BmpHeader::BmpBase) + sizeof(BmpHeader::BmpDib);
}

template <class T>
void BmpHeader::CtxWrite(const T &src)
{
    if (!m_ctx->SetStreamChunk(src, m_writeIndex))
        throw false;

    m_writeIndex += sizeof(T);
}

bool BmpHeader::WriteHeaderCtx(BmpFormat *ctx)
{
    if (ctx == nullptr)
        return false;

    m_writeIndex = 0;
    m_ctx = ctx;

    try
    {
        CtxWrite(m_bmpbase->m_magic);
        CtxWrite(m_bmpbase->m_filesize);
        CtxWrite(m_bmpbase->m_commitLow);
        CtxWrite(m_bmpbase->m_commitHigh);
        CtxWrite(m_bmpbase->m_pixelOff);

        CtxWrite(m_bmpdib->m_dibHdSize);
        CtxWrite(m_bmpdib->m_bmpWidth);
        CtxWrite(m_bmpdib->m_bmpHeight);
        CtxWrite(m_bmpdib->m_colorPlane);
        CtxWrite(m_bmpdib->m_bits);
        CtxWrite(m_bmpdib->m_compressType);
        CtxWrite(m_bmpdib->m_pixelSize);
        CtxWrite(m_bmpdib->m_screenX);
        CtxWrite(m_bmpdib->m_screenY);
        CtxWrite(m_bmpdib->m_paletcol);
        CtxWrite(m_bmpdib->m_tpyIgnore);
    }
    catch (const bool &)
    {
        return false;
    }

    return true;
}


