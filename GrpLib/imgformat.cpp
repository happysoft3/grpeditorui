
#include "imgformat.h"
#include "pixelbuffer.h"

ImgFormat::ImgFormat()
    : BinaryFile()
{
    m_imgFmtType = ImgFormatType::ImgNone;
}

ImgFormat::~ImgFormat()
{ }

bool ImgFormat::Read()
{
    return false;
}

bool ImgFormat::Write()
{
    return false;
}

void ImgFormat::ShareBuffer(ImgFormat *other)
{
    if (other == nullptr)
        return;

    other->m_pixelbuffer = m_pixelbuffer;
}

bool ImgFormat::GetPixelSize(uint32_t &destwidth, uint32_t &destheight)
{
    if (!m_pixelbuffer)
        return false;

    destwidth = m_pixelbuffer->PixelWidth();
    destheight = m_pixelbuffer->PixelHeight();
    return true;
}

