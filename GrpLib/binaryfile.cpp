
#include "binaryfile.h"
#include <fstream>
#include <memory>
#include <functional>

BinaryFile::BinaryFile(const std::string &fileurl)
{
    m_filename = fileurl;
    m_otherCtx = nullptr;
    m_ctxOffset = 0;
}

BinaryFile::~BinaryFile()
{ }

bool BinaryFile::ReadWithFilename(const std::string &fileurl)
{
    if (m_filename.empty())
        return false;

    using binary_ifstream = std::basic_ifstream<uint8_t, std::char_traits<uint8_t>>;
    binary_ifstream file(fileurl.empty() ? m_filename : fileurl, std::ios::binary);

    if (!file.is_open())
        return false;

    static constexpr std::streamoff zero_off = 0;
    file.seekg(zero_off, std::ios::end);
    size_t filesize = static_cast<size_t>(file.tellg());
    file.seekg(zero_off, std::ios::beg);

    m_streambuffer.reserve(filesize + sizeof(size_t));
    m_streambuffer.resize(filesize);
    return file.read(m_streambuffer.data(), m_streambuffer.size()) ? true : false;
}

bool BinaryFile::WriteWithFilename(const std::string &fileurl)
{
    if (m_filename.empty())
        return false;

    if (m_streambuffer.empty())
        return false;

    using binary_ofstream = std::basic_ofstream<uint8_t, std::char_traits<uint8_t>>;
    binary_ofstream file(fileurl.empty() ? m_filename : fileurl, std::ios::binary);

    return file.write(m_streambuffer.data(), m_streambuffer.size()) ? true : false;
}

std::string BinaryFile::FilenameOnly(bool compriseExtension)
{
    static constexpr size_t zero_pos = 0;
    auto findchar = [this](const size_t &findoff)
    {
        return findoff == std::string::npos ? zero_pos : findoff;
    };
    auto findBackshash = findchar(m_filename.find_last_of('\\'));

    if (findBackshash != zero_pos)
        ++findBackshash;
    if (compriseExtension)
        return m_filename.substr(findBackshash);

    auto findDot = findchar(m_filename.find_last_of('.'));

    return m_filename.substr(findBackshash, findDot != zero_pos ? findDot - findBackshash : std::string::npos);
}

bool BinaryFile::PushEOF()
{
    if (m_streambuffer.size() < m_streambuffer.capacity())
    {
        m_streambuffer.push_back(0);
        return true;
    }
    return false;
}

void BinaryFile::SetBufferSize(const size_t &size)
{
    m_streambuffer.clear();
    m_streambuffer.resize(size);
}

void BinaryFile::SetCtx(BinaryFile * ctx, uint32_t offset, uint32_t reSize)
{
    m_otherCtx = ctx;
    m_ctxOffset = offset;

    if (reSize != 0)
        SetBufferSize(reSize);
}

bool BinaryFile::Read()
{
    if (m_otherCtx == nullptr)
        return BinaryFile::ReadWithFilename();

    std::unique_ptr<int, std::function<void(int *)>> clearvar(new int, [this](int *p) { delete p; m_otherCtx = nullptr; m_ctxOffset = 0; });
    uint32_t streamLength = BufferSize();
    uint32_t streamPos = 0;

    while (streamLength--)
    {
        uint8_t getchunk = 0;

        if (!m_otherCtx->GetStreamChunk(getchunk, m_ctxOffset++))
            return false;

        if (!SetStreamChunk(getchunk, streamPos++))
            return false;
    }

    return true;
}

bool BinaryFile::Write()
{
    if (m_otherCtx == nullptr)
        return BinaryFile::WriteWithFilename();

    std::unique_ptr<int, std::function<void(int*)>> clearvar(new int, [this](int *p) { delete p; m_otherCtx = nullptr; m_ctxOffset = 0; });

    for (const auto &uc : m_streambuffer)
    {
        if (!m_otherCtx->SetStreamChunk(uc, m_ctxOffset))
            return false;
        m_ctxOffset += sizeof(uc);
    }
    return true;
}

