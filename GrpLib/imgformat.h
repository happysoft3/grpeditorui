
#ifndef IMG_FORMAT_H__
#define IMG_FORMAT_H__

#include "binaryfile.h"
#include <memory>

class PixelBuffer;

enum class ImgFormatType
{
    ImgNone,
    ImgGrp,
    ImgBmp
};

class ImgFormat : public BinaryFile
{
protected:
    ImgFormatType m_imgFmtType;
    std::shared_ptr<PixelBuffer> m_pixelbuffer;

public:
    ImgFormat();
    ~ImgFormat() override;

public:
    virtual bool Read() override;
    virtual bool Write() override;

    void ShareBuffer(ImgFormat *other);

    ImgFormatType FormatType() const
    {
        return m_imgFmtType;
    }
    bool GetPixelSize(uint32_t &destwidth, uint32_t &destheight);
};

#endif

