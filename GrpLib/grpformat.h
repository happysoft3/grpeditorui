
#ifndef GRP_FORMAT_H__
#define GRP_FORMAT_H__

#include "imgformat.h"
#include <functional>

class PixelBuffer;

class GrpFormat : public ImgFormat
{
private:
    uint32_t m_grpwidth;
    uint32_t m_grpheight;
    uint32_t m_offsetX;
    uint32_t m_offsetY;

    static constexpr uint8_t grp_type_static = 3;
    static constexpr uint8_t grp_pixel_cutter = 2;

    static constexpr uint32_t grp_no_pixel = 0;

    std::vector<uint16_t> m_grpstream;

public:
    explicit GrpFormat(const std::string &filename);
    ~GrpFormat() override;

private:
    uint16_t RgbTo565(uint32_t rgbColor);
    uint32_t Rgb565To888(uint16_t rgb565);
    uint32_t Rgb565To888Scale(uint16_t rgb565);
    bool ComputeValidPixel(uint8_t &dest, uint32_t &index, uint32_t &countdown, std::function<bool(uint32_t)> &&comparer, std::function<void(uint32_t)> &&repeater);
    bool EncordingOneOf(uint32_t &index);
    bool Encording();
    bool GetPixelFromFile(int readindex);

public:
    size_t ComputeGrpFormatTotalLength();

private:
    bool DecordingOf(uint32_t &readpos);
    bool Decording();

public:
    virtual void SetCtx(BinaryFile *ctx, uint32_t offset, uint32_t reSize = 0) override;
    bool Make();
    virtual bool Read() override;
    virtual bool Write() override;
    void GetGrpOffsetInfo(uint32_t &xOff, uint32_t &yOff);
    void SetGrpOffsetInfo(const uint32_t &xOff, const uint32_t &yOff);
};

#endif

