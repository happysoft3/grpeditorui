
#ifndef BINARY_FILE_H__
#define BINARY_FILE_H__

#include <string>
#include <vector>
#include <stdexcept>

class BinaryFile
{
private:
    std::string m_filename;
    std::vector<uint8_t> m_streambuffer;
    BinaryFile *m_otherCtx;
    uint32_t m_ctxOffset;

public:
    explicit BinaryFile(const std::string &fileurl = { });
    virtual ~BinaryFile();

    virtual bool ReadWithFilename(const std::string &fileurl = {});
    virtual bool WriteWithFilename(const std::string &fileurl = {});
    std::string Filename() const
    {
        return m_filename;
    }
    void SetFilename(const std::string &fileurl)
    {
        m_filename = fileurl;
    }
    std::string FilenameOnly(bool compriseExtension = false);

    bool PushEOF();

protected:
    void SetBufferSize(const size_t &size);
    size_t BufferSize() const
    {
        return m_streambuffer.size();
    }

private:
    template <int N>
    struct stream_chunk_modifier
    {
        static constexpr int lshift_count = (N - 1) * 8;

        template <class T>
        static void Get(const std::vector<uint8_t> &buffer, T &dest, int index)
        {
            stream_chunk_modifier<N - 1>::Get(buffer, dest, index - 1);

            dest |= (buffer.at(index) << lshift_count);
        }

        template <class T>
        static void Set(std::vector<uint8_t> &buffer, const T &src, int index)
        {
            stream_chunk_modifier<N - 1>::Set(buffer, src, index - 1);

            buffer.at(index) = (src >> lshift_count) & 0xff;
        }
    };

    template <>
    struct stream_chunk_modifier<1>
    {
        template <class T>
        static void Get(const std::vector<uint8_t> &buffer, T &dest, int index)
        {
            dest |= buffer.at(index);
        }

        template <class T>
        static void Set(std::vector<uint8_t> &buffer, const T &src, int index)
        {
            buffer.at(index) = src & 0xff;
        }
    };

protected:
    template <class T, class = std::enable_if<std::is_arithmetic<T>::value>::type>
    bool GetStreamChunk(T &dest, int index)
    {
        static constexpr int type_size = sizeof(T);

        try
        {
            stream_chunk_modifier<type_size>::Get(m_streambuffer, dest, index + type_size - 1);
        }
        catch (const std::out_of_range &/*oor*/)
        {
            return false;
        }

        return true;
    }

    template <class T, class = std::enable_if<std::is_arithmetic<T>::value>::type>
    bool SetStreamChunk(const T &src, int index)
    {
        static constexpr int type_size = sizeof(T);

        try
        {
            stream_chunk_modifier<type_size>::Set(m_streambuffer, src, index + type_size - 1);
        }
        catch (const std::out_of_range &/*oor*/)
        {
            return false;
        }
        return true;
    }

    virtual void SetCtx(BinaryFile *ctx, uint32_t offset, uint32_t reSize);
    virtual bool Read();
    virtual bool Write();
};

#endif
