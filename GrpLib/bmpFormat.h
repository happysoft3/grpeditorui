
#ifndef BMP_FORMAT_H__
#define BMP_FORMAT_H__

#include "imgformat.h"

//Bmp 24bit only

class BmpHeader;

class BmpFormat : public ImgFormat
{
    friend BmpHeader;

private:
    uint32_t m_endpixelpos;
    uint32_t m_startpixelpos;

    uint32_t m_transparentRedMin;
    uint32_t m_transparentRedMax;
    uint32_t m_transparentBlueMin;
    uint32_t m_transparentBlueMax;
    uint32_t m_transparentGreenMin;
    uint32_t m_transparentGreenMax;

public:
    explicit BmpFormat(const std::string &path);
    ~BmpFormat() override;

private:
    bool Is24Bit();
    uint8_t GetPaddingCount(uint32_t width);
    uint32_t GetRealWidth(uint32_t width);
    void SetTransparentColor(uint8_t transparentRed, uint8_t transparentGreen, uint8_t transparentBlue, uint8_t range);
    void ComputeTransparent(uint32_t &cPixel);
    bool ReadPixelDetail(uint32_t pixelpos);
    bool ReadPixel(uint32_t width, uint32_t height);

    void MakeBitHeader(BmpHeader *&pHeader, uint32_t hintTtsize = 0);

    bool WriteBmpPixelOneOf(uint32_t &curpos, uint32_t wrtpos);
    bool WriteBmpPixel(uint32_t wrtpos);

public:
    bool PixelToBmp();
    virtual bool WriteWithFilename(const std::string &bmpFilename = { }) override;

    virtual bool Read() override;
    virtual bool Write() override;
};

#endif

