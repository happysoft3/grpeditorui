
#ifndef PIXEL_BUFFER_H__
#define PIXEL_BUFFER_H__

#include <vector>

#define PIXEL_BUFFER_EOF  static_cast<uint32_t>(0xffffffff)

class PixelBuffer
{
private:
    uint32_t m_pixelwidth;
    uint32_t m_pixelheight;
    std::vector<uint32_t> m_pixelstream;

public:
    explicit PixelBuffer(uint32_t buffsize = 0);
    ~PixelBuffer();

    void AppendPixel(uint32_t pixel)
    {
        m_pixelstream.push_back(pixel);
    }

    void SetPixelSize(uint32_t width, uint32_t height)
    {
        m_pixelwidth = width;
        m_pixelheight = height;
    }

    uint32_t PixelWidth() const
    {
        return m_pixelwidth;
    }

    uint32_t PixelHeight() const
    {
        return m_pixelheight;
    }

    size_t BufferSize() const
    {
        return m_pixelstream.size();
    }

    bool GetPixel(uint32_t &dest, const size_t &index);
};

#endif

