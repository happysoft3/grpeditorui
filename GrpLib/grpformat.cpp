
#include "grpformat.h"
#include "pixelbuffer.h"

GrpFormat::GrpFormat(const std::string &filename)
    : ImgFormat()
{
    m_imgFmtType = ImgFormatType::ImgGrp;
    m_offsetX = 0;
    m_offsetY = 0;
    m_grpheight = 0;
    m_grpwidth = 0;

    if (filename.length())
        SetFilename(filename);
}

GrpFormat::~GrpFormat()
{ }

uint16_t GrpFormat::RgbTo565(uint32_t rgbColor)
{
    uint32_t red = rgbColor >> 0x10;
    uint32_t grn = (rgbColor >> 0x08) & 0xff;
    uint32_t blu = rgbColor & 0xff;

    return static_cast<uint16_t>(((red >> 3) << 11) | ((grn >> 2) << 5) | (blu >> 3));
}

uint32_t GrpFormat::Rgb565To888(uint16_t rgb565)
{
    uint32_t red = rgb565 & 0xf800 >> 11;
    uint32_t grn = (rgb565 & 0x7e0) >> 5;
    uint32_t blu = rgb565 & 0x1f;

    if (red)
        red = (red == 0x1f) ? 0xff : (red + 1) << 3;
    if (grn)
        grn = (grn == 0x3f) ? 0xff : (grn + 1) << 2;
    if (blu)
        blu = (blu == 0x1f) ? 0xff : (blu + 1) << 3;

    return (0xff << 0x18) | (red << 0x10) | (grn << 8) | blu;
}

uint32_t GrpFormat::Rgb565To888Scale(uint16_t src)
{
    uint32_t b = static_cast<uint8_t>((src & 0xFC00) >> 10);
    uint32_t g = static_cast<uint8_t>((src & 0x03E0) >> 5);
    uint32_t r = static_cast<uint8_t>(src & 0x1F);

    r <<= 3;
    g <<= 3;
    b <<= 3;

    return static_cast<uint32_t>(0xFF000000) | (static_cast<uint32_t>(b) << 16) | (static_cast<uint32_t>(g) << 8) | static_cast<uint32_t>(r);
}

bool GrpFormat::ComputeValidPixel(uint8_t &dest, uint32_t &index, uint32_t &countdown, std::function<bool(uint32_t)> &&comparer, std::function<void(uint32_t)> &&repeater)
{
    uint32_t getpixel = 0;
    uint32_t count = 0;

    while (m_pixelbuffer->GetPixel(getpixel, index++))
    {
        while (true)
        {
            if (comparer(getpixel))
            {
                if ((--countdown) == -1)
                    countdown = 0;
                else if (count < 0x100)
                    break;
            }
            --index;
            dest = static_cast<uint8_t>(count);
            return true;
        }
        repeater(getpixel);
        ++count;
    }
    return false;
}

bool GrpFormat::EncordingOneOf(uint32_t &index)
{
    uint32_t pixel = 0;
    uint8_t validPixcount = 0;
    std::vector<uint32_t> validpixelTemp;
    uint32_t row = m_grpwidth;

    validpixelTemp.reserve(0x100);

    while (row != 0)
    {
        if (!m_pixelbuffer->GetPixel(pixel, index))
            return false;

        if (pixel == grp_no_pixel)
        {
            if (!ComputeValidPixel(validPixcount, index, row, [](uint32_t pix) { return !pix; }, [](uint32_t) { }))
                return false;
            m_grpstream.push_back(1 | (validPixcount << 8));
        }
        else if (pixel == PIXEL_BUFFER_EOF)
            break;
        else
        {
            validpixelTemp.clear();
            if (!ComputeValidPixel(validPixcount, index, row,
                [](uint32_t pix) { return pix != 0 && pix != PIXEL_BUFFER_EOF; }, [&validpixelTemp](uint32_t pus) { validpixelTemp.push_back(pus); }))
                return false;
            m_grpstream.push_back(2 | (validPixcount << 8));
            for (const auto &chunk : validpixelTemp)
                m_grpstream.push_back(RgbTo565(chunk));
        }
    }
    return true;
}

bool GrpFormat::Encording()
{
    uint32_t columns = m_grpheight;
    uint32_t progress = 0;

    while (columns--)
    {
        if (!EncordingOneOf(progress))
            return false;
    }

    m_grpstream.push_back(0);   //push null terminate
    return true;
}

void GrpFormat::SetCtx(BinaryFile * ctx, uint32_t offset, uint32_t reSize)
{
    BinaryFile::SetCtx(ctx, offset, reSize);
}

bool GrpFormat::Make()
{
    if (!m_pixelbuffer)
        return false;

    m_grpwidth = m_pixelbuffer->PixelWidth();
    m_grpheight = m_pixelbuffer->PixelHeight();

    m_grpstream.clear();
    m_grpstream.reserve(m_grpwidth * m_grpheight + 1);

    return Encording();
}

bool GrpFormat::GetPixelFromFile(int readindex)
{
    size_t buffsize = BufferSize();

    m_grpstream.reserve(buffsize - readindex);

    int progress = (buffsize - readindex) / sizeof(decltype(m_grpstream.front()));

    while (progress--)
    {
        uint16_t pix = 0;
        if (!GetStreamChunk(pix, readindex))
            return false;

        m_grpstream.push_back(pix);
        readindex += 2;
    }
    return true;
}

size_t GrpFormat::ComputeGrpFormatTotalLength()
{
    return sizeof(m_grpwidth) + sizeof(m_grpheight) + sizeof(m_offsetX) + sizeof(m_offsetY) + 
        sizeof(grp_type_static) + (m_grpstream.size()*sizeof(decltype(m_grpstream.front())));
}

bool GrpFormat::DecordingOf(uint32_t &readpos)
{
    uint32_t accumulate = 0;

    while (accumulate < m_grpwidth)
    {
        try
        {
            auto rleHead = m_grpstream.at(readpos++);
            uint8_t rleType = rleHead & 0xff;
            uint8_t rleLength = rleHead >> 8;

            accumulate += rleLength;

            if (rleType == 1)   //repe
            {
                while (rleLength--)
                    m_pixelbuffer->AppendPixel(0xff00ff);
            }
            else if (rleType == 2) //valid
            {
                while (rleLength--)
                    m_pixelbuffer->AppendPixel(Rgb565To888Scale(m_grpstream.at(readpos++)));
            }
            else
                return false;   //invalid type
        }
        catch (const std::out_of_range &)
        {
            return false;
        }
    }
    return accumulate == m_grpwidth;
}

bool GrpFormat::Decording()
{
    if (m_grpstream.empty())
        return false;

    m_pixelbuffer = std::make_shared<PixelBuffer>((m_grpwidth * m_grpheight) + sizeof(uint8_t));
    m_pixelbuffer->SetPixelSize(m_grpwidth, m_grpheight);

    uint32_t columns = m_grpheight;
    uint32_t readpos = 0;

    while (columns--)
    {
        if (!DecordingOf(readpos))
            return false;
    }
    return true;
}

bool GrpFormat::Read()
{
    if (!BinaryFile::Read())
        return false;

    std::vector<uint32_t *> varlist({ &m_grpwidth, &m_grpheight, &m_offsetX, &m_offsetY });
    int hexaRep = 0;
    auto range_checker = [](uint32_t value) { return value < 0x200; };

    for (uint32_t *var : varlist)
    {
        if (!GetStreamChunk(*var, hexaRep))
            return false;
        if (!range_checker(*var))
            return false;

        hexaRep += sizeof(*var);
    }

    uint8_t grptype = 0;
    if (!GetStreamChunk(grptype, hexaRep++))
        return false;

    if (!GetPixelFromFile(hexaRep))
        return false;

    return Decording();
}

bool GrpFormat::Write()
{
    SetBufferSize(ComputeGrpFormatTotalLength());

    std::vector<uint32_t *> varlist({ &m_grpwidth, &m_grpheight, &m_offsetX, &m_offsetY });
    int wrtpos = 0;

    for (const uint32_t *var : varlist)
    {
        if (!SetStreamChunk(*var, wrtpos))
            return false;

        wrtpos += sizeof(*var);
    }

    if (!SetStreamChunk(grp_type_static, wrtpos++))
        return false;

    for (const auto &grpPixel : m_grpstream)
    {
        if (!SetStreamChunk(grpPixel, wrtpos))
            return false;
        wrtpos += sizeof(grpPixel);
    }
    SetFilename(FilenameOnly() + ".grp");
    return BinaryFile::Write();
}

void GrpFormat::GetGrpOffsetInfo(uint32_t &xOff, uint32_t &yOff)
{
    xOff = m_offsetX;
    yOff = m_offsetY;
}

void GrpFormat::SetGrpOffsetInfo(const uint32_t &xOff, const uint32_t &yOff)
{
    m_offsetX = xOff;
    m_offsetY = yOff;
}