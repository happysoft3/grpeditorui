
#ifndef BMP_HEADER_H__
#define BMP_HEADER_H__

#include <memory>

class BmpFormat;

class BmpHeader
{
    struct BmpBase;
    struct BmpDib;

private:
    std::unique_ptr<BmpBase> m_bmpbase;
    std::unique_ptr<BmpDib> m_bmpdib;
    BmpFormat *m_ctx;
    int m_writeIndex;

public:
    explicit BmpHeader(uint32_t ttsize = 0);
    ~BmpHeader();

    void SetFilesize(uint32_t ttsize);
    void SetPixelSize(uint32_t width, uint32_t height);
    static uint32_t TotalFieldLength();

private:
    template <class T>
    void CtxWrite(const T &src);

public:
    bool WriteHeaderCtx(BmpFormat *ctx);

};

#endif

