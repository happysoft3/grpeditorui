
#include "multiplegrp.h"
#include "grpformat.h"
#include "bmpFormat.h"
#include "partitionpool.h"
#include "stringhelper.h"

using namespace _StringHelper;

struct MultipleGrp::HeaderInfo
{
    friend MultipleGrp;

    static constexpr uint32_t magic_identifier = 0xdeadface;
    uint32_t m_magic;
    uint32_t m_headLength;
    uint8_t m_grpcount;
    uint32_t m_realOff;

    HeaderInfo()
    {
        m_magic = magic_identifier;
        m_grpcount = 0;
        m_realOff = 0;
        m_headLength = sizeof(HeaderInfo);
    }
};

MultipleGrp::MultipleGrp()
    : BinaryFile("output.mgr")
{
    m_header = std::make_unique<HeaderInfo>();
    m_seekpointer = 0;
    m_partpool = std::make_unique<PartitionPool>(32);
}

MultipleGrp::~MultipleGrp()
{
}

bool MultipleGrp::Append(const std::shared_ptr<ImgFormat> &fmt, int &key)
{
    uint8_t nulldata[4] = {};

    if (!m_partpool->PushData(nulldata, sizeof(nulldata), key))
        return false;
    FormatElement *element = new FormatElement;

    element->m_index = key;
    element->m_fmt = fmt;

    m_fmtList.emplace_back(element);
    m_keymap.emplace(key, --m_fmtList.crbegin().base());
    return true;
}

bool MultipleGrp::Erase(const int &key)
{
    auto keyIterator = m_keymap.find(key);

    if (keyIterator == m_keymap.cend())
        return false;

    m_partpool->ReturnData(key);
    m_fmtList.erase(keyIterator->second);
    m_keymap.erase(keyIterator);
    return true;
}

bool MultipleGrp::GetFirstImg(std::shared_ptr<ImgFormat>& destImg)
{
    if (m_fmtList.empty())
        return false;

    destImg = m_fmtList.front()->m_fmt;
    return true;
}

bool MultipleGrp::GetSelectImage(std::shared_ptr<ImgFormat>& destImg, const int &key)
{
    if (m_fmtList.empty())
        return false;

    auto keyIterator = m_keymap.find(key);

    if (keyIterator == m_keymap.cend())
        return false;

    destImg = (*keyIterator->second)->m_fmt;
    return true;
}

template <class T>
void MultipleGrp::CtxWrite(const T &src)
{
    if (!SetStreamChunk(src, m_seekpointer))
        throw false;

    m_seekpointer += sizeof(T);
}

template <class T>
void MultipleGrp::CtxRead(T &dest)
{
    if (!GetStreamChunk(dest, m_seekpointer))
        throw false;

    m_seekpointer += sizeof(T);
}

void MultipleGrp::GrpListCleanup()
{
    m_grps.clear();
    m_cvgrps.clear();
    m_grpOffsetList.clear();
}

uint32_t MultipleGrp::GetGrpTotalSize()
{
    uint32_t ttsize = 0;

    for (const auto &length : m_grpOffsetList)
        ttsize += length;

    return ttsize;
}

bool MultipleGrp::MakeGrpList()
{
    if (m_fmtList.empty())
        return false;

    GrpListCleanup();

    for (auto &element : m_fmtList)
    {
        auto &fmt = element->m_fmt;
        GrpFormat *grp = nullptr;

        if (fmt->FormatType() == ImgFormatType::ImgBmp)
        {
            std::unique_ptr<GrpFormat> localGrp(new GrpFormat({}));

            fmt->ShareBuffer(localGrp.get());
            if (!localGrp->Make())
                return false;

            grp = localGrp.release();
            m_cvgrps.emplace_back(grp);
        }
        else if (fmt->FormatType() == ImgFormatType::ImgGrp)
            grp = dynamic_cast<GrpFormat *>(fmt.get());

        if (grp == nullptr)
            return false;

        m_grpOffsetList.push_back(grp->ComputeGrpFormatTotalLength());
        m_grps.push_back(grp);
    }

    return true;
}

bool MultipleGrp::WriteGrpOffsetTable()
{
    m_seekpointer = m_header->m_headLength;

    try
    {
        for (auto &off : m_grpOffsetList)
            CtxWrite(off);
    }
    catch (const bool &except)
    {
        return except;
    }
    return true;
}

bool MultipleGrp::BuildHeader()
{
    if (m_fmtList.empty())
        return false;

    uint32_t fmtcount = m_fmtList.size();

    if (fmtcount > 128)
        return false;

    uint32_t offtablesize = fmtcount * sizeof(int *);
    
    m_header->m_grpcount = static_cast<uint8_t>(fmtcount);
    m_header->m_realOff = m_header->m_headLength + offtablesize;

    uint32_t grpTotalSize = GetGrpTotalSize();

    SetBufferSize(m_header->m_headLength + offtablesize + grpTotalSize);
    m_seekpointer = 0;

    try
    {
        CtxWrite(m_header->m_magic);
        CtxWrite(m_header->m_headLength);
        CtxWrite(m_header->m_grpcount);
        CtxWrite(m_header->m_realOff);
    }
    catch (const bool &ret)
    {
        return ret;
    }

    return WriteGrpOffsetTable();
}

bool MultipleGrp::BuildContext()
{
    uint32_t accumulate = m_seekpointer;
    std::list<uint32_t>::const_iterator offsetIter = m_grpOffsetList.cbegin();

    for (auto &grp : m_grps)
    {
        grp->SetCtx(this, accumulate);
        if (!grp->Write())
            return false;
        accumulate += *offsetIter;
        ++offsetIter;
    }
    return true;
}

bool MultipleGrp::ReadHeader()
{
    try
    {
        CtxRead(m_header->m_magic);
        CtxRead(m_header->m_headLength);
        CtxRead(m_header->m_grpcount);
        CtxRead(m_header->m_realOff);
    }
    catch (const bool &ec)
    {
        return ec;
    }
    if (m_header->m_magic != HeaderInfo::magic_identifier)
        return false;
    if (!m_header->m_grpcount)
        return false;

    return true;
}

bool MultipleGrp::ReadGrpOffTable()
{
    uint32_t grpcount = m_header->m_grpcount;
    uint32_t grplength = 0;

    while (grpcount--)
    {
        grplength = 0;
        try
        {
            CtxRead(grplength);
        }
        catch (const bool &ec)
        {
            return ec;
        }
        m_grpOffsetList.push_back(grplength);
    }
    return true;
}

bool MultipleGrp::ReadGrpStream()
{
    std::string grpname = "extract";
    int increase = 0;
    int discard = 0;

    for (const auto &off : m_grpOffsetList)
    {
        std::unique_ptr<GrpFormat> grp(new GrpFormat(stringFormat("%s%03d.grp", grpname, ++increase)));

        grp->SetCtx(this, m_seekpointer, off);
        if (!grp->Read())
            return false;
        
        if (!Append(std::unique_ptr<ImgFormat>(grp.release()), discard))
            return false;
        m_seekpointer += off;
    }
    return true;
}

bool MultipleGrp::Read()
{
    if (!BinaryFile::Read())
        return false;

    GrpListCleanup();
    m_seekpointer = 0;

    if (!ReadHeader())
        return false;

    m_seekpointer = m_header->m_headLength;
    if (!ReadGrpOffTable())
        return false;
    m_seekpointer = m_header->m_realOff;
    if (!ReadGrpStream())
        return false;

    return true;
}

bool MultipleGrp::WriteWithFilename(const std::string &fileurl)
{
    if (m_fmtList.empty())
        return false;

    if (!MakeGrpList())
        return false;

    if (!BuildHeader())
        return false;

    if (!BuildContext())
        return false;

    if (Filename().empty())
        SetFilename(fileurl);

    return Write();
}

std::list<std::tuple<std::string, int>> MultipleGrp::GetFormatList()
{
    if (m_fmtList.empty())
        return{ };

    std::list<std::tuple<std::string, int>> fmts;

    for (auto &element : m_fmtList)
    {
        auto &fmt = element->m_fmt;
        
        fmts.emplace_back(fmt->FilenameOnly(true), element->m_index);
    }
    return fmts;
}

