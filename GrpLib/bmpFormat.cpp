
#include "bmpFormat.h"
#include "pixelbuffer.h"
#include "bmpheader.h"
#include <iostream>

BmpFormat::BmpFormat(const std::string &path)
    : ImgFormat()
{
    m_imgFmtType = ImgFormatType::ImgBmp;
    m_startpixelpos = 0;
    m_endpixelpos = 0;
    SetTransparentColor(255, 0, 255, 3);

    if (path.length())
        SetFilename(path);
}

BmpFormat::~BmpFormat()
{ }

bool BmpFormat::Is24Bit()
{
    short bmpMagic = 0;

    if (!GetStreamChunk(bmpMagic, 0))
        return false;
    if (bmpMagic == 0x4d42)
    {
        int bmpbit = 0;

        if (!GetStreamChunk(bmpbit, 28))
            return false;

        return bmpbit == 0x18;
    }
    return false;
}

uint8_t BmpFormat::GetPaddingCount(uint32_t width)
{
    uint32_t oneLine = width * 3;

    return ((oneLine % 4) ? (4 - (oneLine % 4)) : 0);
}

uint32_t BmpFormat::GetRealWidth(uint32_t width)
{
    return (width*3) + GetPaddingCount(width);
}

void BmpFormat::SetTransparentColor(uint8_t transparentRed, uint8_t transparentGreen, uint8_t transparentBlue, uint8_t range)
{
    auto setTransparentMinPrivate = [range](const uint8_t &transpcol)
    {
        return ((transpcol - range) < 0) ? 0 : (transpcol - range);
    };
    auto setTransparentMaxPrivate = [range](const uint8_t &transpcol)
    {
        return ((transpcol + range) > 255) ? 255 : (transpcol + range);
    };

    m_transparentRedMin = setTransparentMinPrivate(transparentRed);
    m_transparentRedMax = setTransparentMaxPrivate(transparentRed);

    m_transparentGreenMin = setTransparentMinPrivate(transparentGreen) << 8;
    m_transparentGreenMax = setTransparentMaxPrivate(transparentGreen) << 8;

    m_transparentBlueMin = setTransparentMinPrivate(transparentBlue) << 16;
    m_transparentBlueMax = setTransparentMaxPrivate(transparentBlue) << 16;
}

void BmpFormat::ComputeTransparent(uint32_t &cPixel)
{
    auto transparentChecker = [](uint32_t cColor, uint32_t cCmpmin, uint32_t cCmpmax)
    {
        return cColor >= cCmpmin && cColor <= cCmpmax;
    };

    uint32_t redField = cPixel & 0xff;
    uint32_t grnField = cPixel & 0xff00;
    uint32_t bluField = cPixel & 0xff0000;

    if (transparentChecker(redField, m_transparentRedMin, m_transparentRedMax)
        && transparentChecker(bluField, m_transparentBlueMin, m_transparentBlueMax)
        && transparentChecker(grnField, m_transparentGreenMin, m_transparentGreenMax))
    {
        cPixel = 0;
    }
}

bool BmpFormat::ReadPixelDetail(uint32_t pixelpos)
{
    const uint8_t alphaColor = 0x7f;
    const uint32_t colormask = 0xffffff;
    const uint32_t width = m_pixelbuffer->PixelWidth();

    for (uint32_t i = 0; i < width ; ++i)
    {
        uint32_t cPixel = 0;

        if (!GetStreamChunk(cPixel, (i * 3) + pixelpos))
            return false;

        cPixel = ((cPixel & colormask) == 0) ? ((cPixel + 0x060606) & colormask) : (cPixel & colormask);
        ComputeTransparent(cPixel);
        m_pixelbuffer->AppendPixel(cPixel | ((cPixel > 0) ? (alphaColor << 24) : 0));
    }
    return true;
}

bool BmpFormat::ReadPixel(uint32_t width, uint32_t height)
{
    uint16_t pixelFieldsize = 0;

    if (!GetStreamChunk(pixelFieldsize, 34))
        return false;

    uint32_t realWidth = GetRealWidth(width);
    uint32_t backpos = m_endpixelpos;

    m_pixelbuffer = std::make_shared<PixelBuffer>(backpos - m_startpixelpos);
    m_pixelbuffer->SetPixelSize(width, height);

    while (height--)
    {
        backpos -= realWidth;
        if (!ReadPixelDetail(backpos))
            return false;
    }
    m_pixelbuffer->AppendPixel(PIXEL_BUFFER_EOF);
    return true;
}

void BmpFormat::MakeBitHeader(BmpHeader *&pHeader, uint32_t hintTtsize)
{
    BmpHeader *header = new BmpHeader(hintTtsize);

    header->SetPixelSize(m_pixelbuffer->PixelWidth(), m_pixelbuffer->PixelHeight());

    if (hintTtsize)
        header->SetFilesize(hintTtsize);
    pHeader = header;
}

bool BmpFormat::WriteBmpPixelOneOf(uint32_t &curpos, uint32_t wrtpos)
{
    uint32_t row = m_pixelbuffer->PixelWidth();

    while (row --)
    {
        uint32_t cPixel = 0;

        if (!m_pixelbuffer->GetPixel(cPixel, curpos++))
            return false;
        uint8_t first = cPixel & 0xff;
        uint8_t second = static_cast<uint8_t>(cPixel >> 8);
        uint8_t last = static_cast<uint8_t>(cPixel >> 0x10);

        if (!SetStreamChunk(first, wrtpos++))
            return false;
        if (!SetStreamChunk(second, wrtpos++))
            return false;
        if (!SetStreamChunk(last, wrtpos++))
            return false;
    }
    uint8_t padding = GetPaddingCount(m_pixelbuffer->PixelWidth());
    uint8_t paddingByte = 0;

    while (padding--)
    {
        if (!SetStreamChunk(paddingByte, wrtpos++))
            return false;
    }
    return true;
}

bool BmpFormat::WriteBmpPixel(uint32_t wrtpos)
{
    uint32_t columns = m_pixelbuffer->PixelHeight();
    uint32_t curpos = 0;
    uint32_t rwidth = GetRealWidth(m_pixelbuffer->PixelWidth());

    while (columns--)
    {
        wrtpos -= rwidth;
        if (!WriteBmpPixelOneOf(curpos, wrtpos))
            return false;
    }
    return true;
}

bool BmpFormat::PixelToBmp()
{
    if (!m_pixelbuffer)
        return false;

    uint32_t ttsize = BmpHeader::TotalFieldLength() + (GetRealWidth(m_pixelbuffer->PixelWidth()) * m_pixelbuffer->PixelHeight());
    BmpHeader *pHeader = nullptr;

    SetBufferSize(ttsize);
    MakeBitHeader(pHeader, ttsize);

    std::unique_ptr<BmpHeader> bitHeader(pHeader);
    
    if (!bitHeader->WriteHeaderCtx(this))
        return false;

    return WriteBmpPixel(ttsize);
}

bool BmpFormat::WriteWithFilename(const std::string &bmpFilename)
{
    if (bmpFilename.empty())
        return BinaryFile::WriteWithFilename(FilenameOnly() + ".bmp");

    return BinaryFile::WriteWithFilename(bmpFilename);
}

bool BmpFormat::Read()
{
    if (!BinaryFile::Read())
        return false;
    if (!PushEOF())
        return false;

    if (!Is24Bit())
        return false;

    uint32_t width = 0, height = 0;
    GetStreamChunk(m_startpixelpos, 10);
    GetStreamChunk(m_endpixelpos, 2);
    GetStreamChunk(width, 18);
    GetStreamChunk(height, 22);

    if (!m_startpixelpos || !height || !width || !m_endpixelpos)
        return false;

    return ReadPixel(width, height);
}

bool BmpFormat::Write()
{
    return BinaryFile::Write();
}

