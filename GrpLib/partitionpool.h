
#ifndef PARTITION_POOL_H__
#define PARTITION_POOL_H__

#include <stack>
#include <vector>
#include <memory>

class PartitionPool
{
    using chunk_type = uint8_t;
    class Partition;
private:
    size_t m_size;
    std::stack<int> m_stack;
    std::vector<Partition> m_mempool;

    size_t m_largebuffersize;
    std::unique_ptr<chunk_type[]> m_largebuffer;

public:
    explicit PartitionPool(size_t count = 0);
    ~PartitionPool();

    void Resize(size_t count);
    void AllocateInnerbuffer(size_t allocsize);

private:
    bool Pop(int &dest);
    Partition *GetPart(int index);
    bool CheckCapacity(size_t ssize);

public:
    bool PushData(chunk_type *stream, size_t ssize, int &getindex);
    bool GetData(const int &index, chunk_type *destStream);
    void ReturnData(const int &index);

private:
    bool Peek(int &index);
};

#endif

