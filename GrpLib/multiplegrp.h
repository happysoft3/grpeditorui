
#ifndef MULTIPLE_GRP_H__
#define MULTIPLE_GRP_H__

#include "binaryfile.h"

#include <list>
#include <memory>
#include <map>

class ImgFormat;
class GrpFormat;
class PartitionPool;

class MultipleGrp : public BinaryFile
{
private:
    struct FormatElement
    {
        int m_index;
        std::shared_ptr<ImgFormat> m_fmt;
    };

    struct HeaderInfo;
    using format_element_type = std::shared_ptr<FormatElement>;
    using format_list_type = std::list<format_element_type>;
    using format_list_iterator = format_list_type::const_iterator;

private:
    format_list_type m_fmtList;
    std::list<GrpFormat *> m_grps;
    std::list<std::unique_ptr<GrpFormat>> m_cvgrps;
    std::list<uint32_t> m_grpOffsetList;
    std::unique_ptr<HeaderInfo> m_header;
    uint32_t m_seekpointer;
    std::map<int, format_list_iterator> m_keymap;
    std::unique_ptr<PartitionPool> m_partpool;

public:
    explicit MultipleGrp();
    ~MultipleGrp() override;

    bool Append(const std::shared_ptr<ImgFormat> &fmt, int &key);  //리스트에 추가
    bool Erase(const int &key);   //리스트에서 제거
    bool GetFirstImg(std::shared_ptr<ImgFormat> &destImg);
    bool GetSelectImage(std::shared_ptr<ImgFormat> &destImg, const int &key);

private:
    template <class T>
    void CtxWrite(const T &src);
    template <class T>
    void CtxRead(T &dest);
    void GrpListCleanup();

private:
    uint32_t GetGrpTotalSize();
    bool MakeGrpList();
    bool WriteGrpOffsetTable();
    bool BuildHeader();
    bool BuildContext();
    bool ReadHeader();
    bool ReadGrpOffTable();
    bool ReadGrpStream();

public:
    virtual bool Read() override;
    virtual bool WriteWithFilename(const std::string &fileurl = {}) override;   //멀티grp 쓰기
    std::list<std::tuple<std::string, int>> GetFormatList();
};

#endif

