
#include "stdafx.h"
#include "subpage.h"
#include "eventworker.h"
#include "multiplegrp.h"
#include "stringhelper.h"
#include "resource.h"

#include <sstream>

using namespace _StringHelper;


SubPage::SubPage(UINT nIDTemplate, CWnd *parent)
    : InteractPage(nIDTemplate, parent)
{
    m_receiver = std::make_unique<EventReceiver>(this);
}

SubPage::~SubPage()
{
}

void SubPage::OnInitialUpdate()
{
    InteractPage::OnInitialUpdate();

    m_btnAdd.SetCallback([this]() { this->OnAddClick(); });
    m_btnDel.SetCallback([this]() { this->OnDeleteClick(); });
    m_btnSave.SetCallback([this]() { this->OnSaveClick(); });
    m_btnSelect.SetCallback([this]() { this->OnSelectClick(); });
}

void SubPage::InitCControls()
{
    m_btnAdd.ModifyWndName("Append");
    m_btnDel.ModifyWndName("Erase");
    m_btnSave.ModifyWndName("SaveAsFile");
    m_btnSelect.ModifyWndName("Reloading");

    CRect viewRect;

    m_listview.GetClientRect(&viewRect);
    int keyFieldWidth = 50;

    m_listview.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_SINGLESEL);
    m_listview.InsertColumn(0, "image", LVCFMT_LEFT, viewRect.Width() - keyFieldWidth);
    m_listview.InsertColumn(1, "key", LVCFMT_LEFT, keyFieldWidth);
}

void SubPage::SendEventMsg(const std::string &msg)
{
    int key = -1;

    if (m_selectedColumn != -1)
    {
        CString keystr = m_listview.GetItemText(m_selectedColumn, SubPageListItem::int_index);
        std::stringstream ss(keystr.GetBuffer());

        ss >> key;
    }

    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnButtonClicked, msg, key);
}

void SubPage::UpdateMultiGrpList(const std::list<SubPageListItem> &items)
{
    std::unique_ptr<int, std::function<void(int*)>> resetDraw(new int, [this](int *p) { delete p; m_listview.SetRedraw(true); });

    m_listview.SetRedraw(false);
    m_listview.DeleteAllItems();

    if (items.empty())
        return;

    int column = 0;
    for (const auto &item : items)
    {
        m_listview.InsertItem(column, toArray(item.m_content));
        m_listview.SetItemText(column, SubPageListItem::int_index, toArray(std::to_string(item.m_int)));
        ++column;
    }
}

void SubPage::DoDataExchange(CDataExchange *pDX)
{
    CTabPage::DoDataExchange(pDX);
    auto controlExchange = [pDX](int nIDC, CWnd &wnd)
    { DDX_Control(pDX, nIDC, wnd); };

    controlExchange(SUBPAGE_ADD, m_btnAdd);
    controlExchange(SUBPAGE_DEL, m_btnDel);
    controlExchange(SUBPAGE_SAVE, m_btnSave);
    controlExchange(SUBPAGE_LIST, m_listview);
    controlExchange(SUBPAGE_SELECT, m_btnSelect);

    InitCControls();
}

void SubPage::OnChangedSelecting(NMHDR * pNMHDR, LRESULT * pResult)
{
    NM_LISTVIEW *pNMListview = reinterpret_cast<NM_LISTVIEW*>(pNMHDR);

    if ((pNMListview->uChanged & LVIF_STATE) && (pNMListview->uNewState & LVIS_SELECTED))
        m_selectedColumn = pNMListview->iItem;
}

void SubPage::OnAddClick()
{
    SendEventMsg("add");
}

void SubPage::OnDeleteClick()
{
    SendEventMsg("del");
}

void SubPage::OnSaveClick()
{
    SendEventMsg("save");
}

void SubPage::OnSelectClick()
{
    SendEventMsg("reload");
}

BEGIN_MESSAGE_MAP(SubPage, InteractPage)
    ON_NOTIFY(LVN_ITEMCHANGED, SUBPAGE_LIST, OnChangedSelecting)
END_MESSAGE_MAP()


void SubPage::EventReceiver::DoUpdateSubPageList(const std::list<SubPageListItem>& items)
{
    m_parent->UpdateMultiGrpList(items);
}
