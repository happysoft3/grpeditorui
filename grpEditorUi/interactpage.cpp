
#include "stdafx.h"
#include "interactpage.h"

class InteractPage::PageReceiver
{
private:
    InteractPage *m_parent;

public:
    explicit PageReceiver(InteractPage *parent)
    {
        m_parent = parent;
    }
};

InteractPage::InteractPage(UINT _nIDTemplate, CWnd *parent)
    : CTabPage(_nIDTemplate, parent)
{
    m_receiver = std::make_unique<PageReceiver>(this);
}

InteractPage::~InteractPage()
{
}

void InteractPage::OnInitialUpdate()
{
    CTabPage::OnInitialUpdate();
}

void InteractPage::OnEnterScreen()
{
    m_OnScreenVisibleChanged.Emit(m_pagename, true);
}

void InteractPage::OnExitScreen()
{
    m_OnScreenVisibleChanged.Emit(m_pagename, false);
}
