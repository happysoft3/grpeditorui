
#ifndef SELECT_FILE_DLG_H__
#define SELECT_FILE_DLG_H__

#include "prettybutton.h"
#include "eventsignalslot.h"
#include <string>

class NotifyBoxDlg;

class SelectFileDlg : public CDialogEx
{
private:
    CWnd *m_parent;
    PrettyButton m_btnOk;
    PrettyButton m_moveToUp;
    PrettyButton m_btnClose;
    CListCtrl m_listview;
    CStatic m_displaypath;
    std::string m_currentPath;
    std::string m_selectedItemName;
    std::unique_ptr<CImageList> m_imgList;
    std::shared_ptr<NotifyBoxDlg> m_notifybox;

private:
    static constexpr int file_list_column_img = 0;
    static constexpr int file_list_column_name = 1;

public:
    SelectFileDlg(CWnd *parent = nullptr);

private:
    void SetControlsCallback();

protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


                                                        // 구현입니다.
private:
    std::string GetDefaultPath();
    void InitializeControls();
    void UpdateFilelist();
    void GoDirectory(const std::string &dir);

public:
    void SetNotifyBox(const std::shared_ptr<NotifyBoxDlg> &notifybox);
    void ShowWnd();

private:
    void CloseWnd();

protected:
    HICON m_hIcon;

    // 생성된 메시지 맵 함수
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg void OnChangedSelecting(NMHDR *pNMHDR, LRESULT *pResult);
    DECLARE_MESSAGE_MAP()

private:
    void OnClickedChoose();
    void OnClickedMoveToUp();
    void OnClickedClose();

    DECLARE_SIGNAL(OnSelectedFile, std::string)
};

#endif

