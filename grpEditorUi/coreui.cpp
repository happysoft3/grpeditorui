
#include "stdafx.h"
#include "coreui.h"
#include "grpEditorUiDlg.h"
#include "notifyboxdlg.h"
#include "multiplegrp.h"
#include "bmpFormat.h"
#include "grpformat.h"
#include "pagemanager.h"
#include "eventworker.h"

using namespace _StringHelper;

CoreUi::CoreUi(CgrpEditorUiDlg *mainwnd)
    : CCObject()
{
    m_mainwnd = mainwnd;
    m_openFormat = FFormat::NONE;

    m_extensionMap.emplace(".bmp", FFormat::BMP);
    m_extensionMap.emplace(".grp", FFormat::GRP);
    m_extensionMap.emplace(".png", FFormat::PNG);
    m_extensionMap.emplace(".mgr", FFormat::MGRP);

    m_grpPack = std::make_unique<MultipleGrp>();
}

CoreUi::~CoreUi()
{ }

bool CoreUi::BmpLoadProcess(const std::string &bmpfile, CRect &destRect)
{
    m_targetFormat = std::shared_ptr<ImgFormat>(new BmpFormat(bmpfile));

    if (!m_targetFormat->Read())
        return false;

    uint32_t width = 0, height = 0;

    if (!m_targetFormat->GetPixelSize(width, height))
        return false;

    destRect.right = width;
    destRect.bottom = height;

    return true;
}

bool CoreUi::GrpLoadProcess(const std::string &grpfile, CRect &destRect)
{
    m_targetFormat = std::shared_ptr<ImgFormat>(new GrpFormat(grpfile));
    std::unique_ptr<BmpFormat> bmp(new BmpFormat("temp.bmp"));

    if (!m_targetFormat->Read())
        return false;

    m_targetFormat->ShareBuffer(bmp.get());
    if (!bmp->PixelToBmp())
        return false;
    if (!bmp->Write())
        return false;

    uint32_t width = 0, height = 0;
    uint32_t xOff = 0, yOff = 0;

    if (!bmp->GetPixelSize(width, height))
        return false;
    static_cast<GrpFormat *>(m_targetFormat.get())->GetGrpOffsetInfo(xOff, yOff);

    destRect.right = width;
    destRect.bottom = height;
    destRect.left = xOff;
    destRect.top = yOff;

    return true;
}

bool CoreUi::MultiGrpLoadProcess(const std::string & mgrpfile)
{
    m_grpPack = std::make_unique<MultipleGrp>();

    m_grpPack->SetFilename(mgrpfile);

    if (!m_grpPack->Read())
        return false;

    if (!m_grpPack->GetFirstImg(m_targetFormat))
        return false;

    if (!ReloadFormat(0))
        return false;

    return UpdateSubpageList();
}

bool CoreUi::CheckFormat(const std::string & extension, FFormat & result)
{
    auto formatIterator = m_extensionMap.find(extension);

    if (formatIterator != m_extensionMap.end())
    {
        result = formatIterator->second;
        return true;
    }
    return false;
}

void CoreUi::OnFileCheckout(const std::string &file)
{
    m_mainwnd->UpdateCurrentFileDisplayer(file);

    auto findDot = file.find_last_of('.');
    std::string fileExtension = file.substr(findDot);
    std::shared_ptr<NotifyBoxDlg> notifybox = m_notifybox.lock();

    if (!notifybox)
        return;

    FFormat form = FFormat::NONE;

    if (!CheckFormat(fileExtension, form))
    {
        m_targetFormat.reset();
        notifybox->ShowNotification("normal", "load failure. not support format!", false);
        m_mainwnd->UpdateImageView("", { });
        return;
    }

    CRect rect;
    bool pResult = false;
    std::string imgurl = "temp.bmp";

    do
    {
        if (form == FFormat::BMP)
        {
            pResult = BmpLoadProcess(file, rect);
            imgurl = file;
        }
        else if (form == FFormat::GRP)
            pResult = GrpLoadProcess(file, rect);
        else if (form == FFormat::MGRP)
        {
            pResult = MultiGrpLoadProcess(file);
            break;
        }

        m_mainwnd->UpdateImageView(imgurl, rect);

        if (pResult)
            ReportChangeOffset("xoff", [](uint32_t n) { return n; });
    }
    while (false);

    if (!pResult)
    {
        notifybox->ShowNotification("normal", stringFormat("failed to load file %s ... %s", file, fileExtension), false);
    }
    else
        notifybox->ShowNotification("normal", stringFormat("loading is done! %s file", file), false);
}

void CoreUi::SetNotifyBox(const std::shared_ptr<NotifyBoxDlg> &notifybox)
{
    if (m_notifybox.expired())
    {
        if (!notifybox)
            return;

        m_notifybox = notifybox;

        notifybox->OnResponse().Connection(&CoreUi::NotifyResponseResult, this);
    }
}

void CoreUi::PutOutput()
{
    if (!m_targetFormat)
        return;

    auto fmtType = m_targetFormat->FormatType();

    if (fmtType == ImgFormatType::ImgBmp)
    {
        std::unique_ptr<GrpFormat> grp(new GrpFormat(m_targetFormat->Filename()));

        m_targetFormat->ShareBuffer(grp.get());
        if (grp->Make())
            grp->Write();
    }
    else if (fmtType == ImgFormatType::ImgGrp)
    {
        static_cast<GrpFormat *>(m_targetFormat.get())->Write();
    }
}

void CoreUi::ReportChangeOffset(const std::string &offsetName, std::function<uint32_t(uint32_t)> &&operation)
{
    if (!m_targetFormat)
        return;

    GrpFormat *grp = dynamic_cast<GrpFormat *>(m_targetFormat.get());

    if (grp == nullptr)
        return;

    uint32_t xOff = 0, yOff = 0, *sel = nullptr;
    grp->GetGrpOffsetInfo(xOff, yOff);

    if (offsetName == "xoff")
        sel = &xOff;
    else if (offsetName == "yoff")
        sel = &yOff;

    if (sel != nullptr)
    {
        *sel = operation(*sel);
        if (*sel & 0xffffff00)
            *sel = 0;
    }
    uint32_t wid = 0, heig = 0;

    grp->GetPixelSize(wid, heig);
    grp->SetGrpOffsetInfo(xOff, yOff);
    m_mainwnd->UpdateImageView(" ", CRect(xOff, yOff, wid, heig), true);

    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnUpdateXYOffset, static_cast<int>(xOff), static_cast<int>(yOff));
}

void CoreUi::InvokeUpdateXYDisp(int x, int y)
{
    if (m_mainwnd == nullptr)
        return;

    m_mainwnd->TeleportXYDisplay(x, y);
}

bool CoreUi::AddFormat()
{
    if (!m_targetFormat)
        return false;

    int key = 0;

    m_grpPack->Append(m_targetFormat, key);
    return true;
}

bool CoreUi::DeleteFormat(int delkey)
{
    if (!m_grpPack->Erase(delkey))
        return false;

    return true;
}

bool CoreUi::ReloadFormat(int selKey)
{
    if (!m_grpPack->GetSelectImage(m_targetFormat, selKey))
        return false;

    uint32_t width, height;
    std::string imgUrl = m_targetFormat->Filename();

    if (!m_targetFormat->GetPixelSize(width, height))
        return false;
    GrpFormat *grp = dynamic_cast<GrpFormat*>(m_targetFormat.get());
    uint32_t xoff = 0, yoff = 0;

    if (grp != nullptr)
    {
        imgUrl = "temp.bmp";
        grp->GetGrpOffsetInfo(xoff, yoff);

        std::unique_ptr<BmpFormat> bmp(new BmpFormat("temp.bmp"));

        m_targetFormat->ShareBuffer(bmp.get());
        if (!bmp->PixelToBmp())
            return false;
        if (!bmp->Write())
            return false;
    }

    m_mainwnd->UpdateImageView(imgUrl, CRect(xoff, yoff, width, height));
    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnUpdateXYOffset, static_cast<int>(xoff), static_cast<int>(yoff));
    return true;
}

bool CoreUi::UpdateSubpageList()
{
    auto items = m_grpPack->GetFormatList();

    std::list<SubPageListItem> updateItem;

    for (const auto &tp : items)
    {
        SubPageListItem itm;

        itm.m_int = std::get<1>(tp);
        itm.m_content = std::get<0>(tp);
        updateItem.push_back(std::move(itm));
    }
    m_OnSendSubPageUpdate.Emit(updateItem);
    return true;
}

void CoreUi::ListenSubPage(const std::string &msg, int key)
{
    if (!m_grpPack)
        return;

    if (msg == "add")
    {
        if (AddFormat())
            UpdateSubpageList();
    }
    else if (msg == "del")
    {
        if (DeleteFormat(key))
            UpdateSubpageList();
    }
    else if (msg == "reload")
    {
        ReloadFormat(key);
    }
    else if (msg == "save")
    {
        m_grpPack->WriteWithFilename();
    }
}

void CoreUi::NotifyResponseResult(const std::string msgid, const std::string result)
{
    if (msgid == "saveformat")
    {
        if (result == "yes")
            PutOutput();
    }
}
