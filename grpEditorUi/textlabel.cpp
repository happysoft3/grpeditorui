
#include "stdafx.h"
#include "TextLabel.h"

IMPLEMENT_DYNAMIC(TextLabel, CStatic)

TextLabel::TextLabel()
    : CStatic(),
    m_textColor(RGB(0, 0, 0)),
    m_bkBrush(std::make_unique<CBrush>(RGB(0, 0, 0)))
{
    m_bkColor = 0;
    m_transparent = true;
}

void TextLabel::SetBackgroundColor(UINT rgb)
{
    m_bkColor = rgb;
    m_bkBrush = std::make_unique<CBrush>(m_bkColor);
    if (m_transparent)
        m_transparent = false;
}

void TextLabel::SetTextColor(UINT rgb)
{
    m_textColor = rgb;
}

BEGIN_MESSAGE_MAP(TextLabel, CStatic)
    ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

HBRUSH TextLabel::CtlColor(CDC *pDC, UINT nCtlColor)
{
    pDC->SetTextColor(m_textColor);

    if (m_transparent)
        pDC->SetBkMode(TRANSPARENT);
    else
        pDC->SetBkColor(m_bkColor);

    return *m_bkBrush;
}

