
#ifndef SUB_PAGE_H__
#define SUB_PAGE_H__

#include "interactpage.h"
#include "subpageitem.h"
#include "prettybutton.h"

class SubPage : public InteractPage
{
public:
    class EventReceiver;
    friend EventReceiver;
private:
    PrettyButton m_btnAdd;
    PrettyButton m_btnDel;
    PrettyButton m_btnSave;
    PrettyButton m_btnSelect;
    CListCtrl m_listview;
    int m_selectedColumn;
    std::shared_ptr<EventReceiver> m_receiver;

public:
    explicit SubPage(UINT nIDTemplate, CWnd *parent);
    virtual ~SubPage() override;

    virtual void OnInitialUpdate() override;
    std::shared_ptr<EventReceiver> Receiver()
    {
        return m_receiver;
    }

private:
    void InitCControls();
    void SendEventMsg(const std::string &msg);

public:
    void UpdateMultiGrpList(const std::list<SubPageListItem> &items);

protected:
    virtual void DoDataExchange(CDataExchange *pDX) override;
    afx_msg void OnChangedSelecting(NMHDR *pNMHDR, LRESULT *pResult);
    DECLARE_MESSAGE_MAP()

private:
    void OnAddClick();
    void OnDeleteClick();
    void OnSaveClick();
    void OnSelectClick();

private:
    DECLARE_SIGNAL(OnButtonClicked, std::string, int)
};

class SubPage::EventReceiver : public CCObject
{
private:
    SubPage *m_parent;

public:
    EventReceiver(SubPage *parent)
        : CCObject()
    {
        m_parent = parent;
    }
    void DoUpdateSubPageList(const std::list<SubPageListItem> &items);
};

#endif

