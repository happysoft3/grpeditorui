
#ifndef INTERACT_PAGE_H__
#define INTERACT_PAGE_H__

#include "ctabpage.h"
#include "eventsignalslot.h"

#include <string>

class InteractPage : public CTabPage
{
    class PageReceiver;
    friend PageReceiver;
private:
    std::string m_pagename;
    std::unique_ptr<PageReceiver> m_receiver;

public:
    explicit InteractPage(UINT _nIDTemplate, CWnd *parent);
    virtual ~InteractPage() override;

    void SetPageName(const std::string &pagename)
    {
        m_pagename = pagename;
    }

    virtual void OnInitialUpdate();

private:
    virtual void OnEnterScreen() override;
    virtual void OnExitScreen() override;

    DECLARE_SIGNAL(OnScreenVisibleChanged, std::string, bool)
};

#endif

