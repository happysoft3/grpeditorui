
#include "stdafx.h"
#include "notifyboxdlg.h"
#include "resource.h"

#include "eventworker.h"

NotifyBoxDlg::NotifyBoxDlg(CWnd *parent)
    : CDialogEx(), m_bkBrush(new CBrush(RGB(84, 80, 58)))
{
    m_parent = parent;
}

void NotifyBoxDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);

    DDX_Control(pDX, NOTIFYBOX_OK, m_btnOk);
    DDX_Control(pDX, NOTIFYBOX_YES, m_btnYes);
    DDX_Control(pDX, NOTIFYBOX_NO, m_btnNo);
    DDX_Control(pDX, NOTIFYBOX_TEXT, m_textWnd);

    m_btnOk.m_bDontUseWinXPTheme = true;
    m_btnYes.m_bDontUseWinXPTheme = true;
    m_btnNo.m_bDontUseWinXPTheme = true;
}

void NotifyBoxDlg::WindowCentering()
{
    CenterWindow();
}

void NotifyBoxDlg::InitializeControls()
{
    m_btnOk.SetWindowTextA("Confirm");
    m_btnYes.SetWindowTextA("Accept");
    m_btnNo.SetWindowTextA("Reject");

    m_textWnd.SetTextColor(RGB(241, 241, 241));
    m_btnOk.SetTextColor(RGB(241, 16, 32));
    m_btnYes.SetTextColor(RGB(241, 16, 32));
    m_btnNo.SetTextColor(RGB(241, 16, 32));
}

void NotifyBoxDlg::ClickButtonCommon()
{
    ShowWindow(SW_HIDE);
    //m_parent->ShowWindow(SW_SHOW);
}

void NotifyBoxDlg::ShowNotification(const std::string msgId, const std::string &text, bool useTwocase)
{
    auto setCtrlVisibleStatus = [](CWnd *ctrl, bool set)
    {
        ctrl->ShowWindow(set ? SW_SHOW : SW_HIDE);
        ctrl->EnableWindow(set);
    };
    m_msgId = msgId;
    setCtrlVisibleStatus(&m_btnOk, !useTwocase);
    setCtrlVisibleStatus(&m_btnYes, useTwocase);
    setCtrlVisibleStatus(&m_btnNo, useTwocase);
    m_textWnd.SetWindowTextA(text.c_str());

    ShowWindow(SW_SHOW);
    SetFocus();
}

BOOL NotifyBoxDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
    //  프레임워크가 이 작업을 자동으로 수행합니다.
    SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
    SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

                                    // TODO: 여기에 추가 초기화 작업을 추가합니다.
    InitializeControls();
    WindowCentering();

    return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

BEGIN_MESSAGE_MAP(NotifyBoxDlg, CDialogEx)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(NOTIFYBOX_OK, OnClickedOK)
    ON_BN_CLICKED(NOTIFYBOX_YES, OnClickedYes)
    ON_BN_CLICKED(NOTIFYBOX_NO, OnClickedNo)
    ON_WM_CTLCOLOR()
    ON_WM_ACTIVATE()
END_MESSAGE_MAP()

void NotifyBoxDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 아이콘을 그립니다.
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

HCURSOR NotifyBoxDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

BOOL NotifyBoxDlg::PreTranslateMessage(MSG* pMsg)
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_RETURN:
        case VK_ESCAPE:
        case VK_F1:
            return TRUE;
        }
    }
    return CDialogEx::PreTranslateMessage(pMsg);
}

HBRUSH NotifyBoxDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    //pDC->SetBkColor(m_bkColor);
    return *m_bkBrush;

}

void NotifyBoxDlg::OnClickedOK()
{
    ClickButtonCommon();

    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnResponse, m_msgId, std::string("ok"));
}

void NotifyBoxDlg::OnClickedYes()
{
    ClickButtonCommon();

    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnResponse, m_msgId, std::string("yes"));
}

void NotifyBoxDlg::OnClickedNo()
{
    ClickButtonCommon();

    EventWorker &worker = EventWorker::Instance();

    worker.AppendTask(&m_OnResponse, m_msgId, std::string("no"));
}

void NotifyBoxDlg::OnActivate(UINT nState, CWnd * pWndOther, BOOL bMinimized)
{
    CDialogEx::OnActivate(nState, pWndOther, bMinimized);

    switch (nState)
    {
    case WA_INACTIVE:   //lost focus
    case WA_ACTIVE:     //get focus
    case WA_CLICKACTIVE:
        break;
    }
}

