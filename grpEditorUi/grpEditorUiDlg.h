
// grpEditorUiDlg.h : 헤더 파일
//

#pragma once

#include <memory>
#include <functional>
#include <string>

#include "textlabel.h"
#include "prettybutton.h"
#include "groupbox.h"

class NotifyBoxDlg;
class SelectFileDlg;
class CoreUi;
class PageManager;

// CgrpEditorUiDlg 대화 상자
class CgrpEditorUiDlg : public CDialogEx
{
private:
    PrettyButton m_btnOpenFile;
    PrettyButton m_btnOutput;
    TextLabel m_filenameDisplay;
    TextLabel m_textParamWidth;
    TextLabel m_textParamHeight;
    TextLabel m_textParamXOff;
    TextLabel m_textParamYOff;
    std::shared_ptr<NotifyBoxDlg> m_notifyboxWnd;
    std::unique_ptr<SelectFileDlg, std::function<void(SelectFileDlg*)>> m_filelistWnd;
    std::unique_ptr<CoreUi> m_coreui;
    std::shared_ptr<PageManager> m_pageman;
    CStatic m_imgview;
    std::unique_ptr<CImage> m_mainimg;
    TextLabel m_dispWidth;
    TextLabel m_dispHeight;
    TextLabel m_dispXOff;
    TextLabel m_dispYOff;
    std::unique_ptr<CRect> m_viewRect;

    GroupBox m_dispPanel;
    GroupBox m_viewPanel;
    GroupBox m_finfoPanel;
    GroupBox m_ctlPanel;

    std::unique_ptr<CBrush> m_bkBrush;
    TextLabel m_viewbase;

    PrettyButton m_btnDecXOff;
    PrettyButton m_btnIncXOff;
    PrettyButton m_btnDecYOff;
    PrettyButton m_btnIncYOff;

    PrettyButton m_btnDec10XOff;
    PrettyButton m_btnInc10XOff;
    PrettyButton m_btnDec10YOff;
    PrettyButton m_btnInc10YOff;

    TextLabel m_xypointDisp;
    PrettyButton m_btnShowXyToggle;

// 생성입니다.
public:
	CgrpEditorUiDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
    ~CgrpEditorUiDlg() override;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GRPEDITORUI_DIALOG };
#endif

private:
    void InitCControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
    void ControlsRegistCallback();
    void Initialize();
    void InitPageManager();
    void DrawImageViewBorder();

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnClose();
    afx_msg BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()

private:
    void OnClickedOpenFile();
    void OnClickedOutput();
    void OnClickedChangeXYOffset(const char *offset, int n);
    //void OnClickedIncreaseXOff(int n);
    //void OnClickedDecreaseYOff(int n);
    //void OnClickedIncreaseYOff(int n);
    void OnClickedToggleXYDotVisible();

public:
    void UpdateCurrentFileDisplayer(const std::string &file);
    void UpdateImageView(const std::string &imgurl, const CRect &rect, bool textonly = false);
    void TeleportXYDisplay(int xpos, int ypos);
};
