
#ifndef NOTIFY_BOX_DLG_H__
#define NOTIFY_BOX_DLG_H__

#include "textlabel.h"
#include "eventsignalslot.h"
#include <string>

class NotifyBoxDlg : public CDialogEx
{
private:
    CWnd *m_parent;
    CMFCButton m_btnOk;
    CMFCButton m_btnYes;
    CMFCButton m_btnNo;
    TextLabel m_textWnd;
    std::string m_msgId;

    std::unique_ptr<CBrush> m_bkBrush;

public:
    NotifyBoxDlg(CWnd *parent = nullptr);

protected:
    virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


                                                        // 구현입니다.    
private:
    void WindowCentering();
    void InitializeControls();
    void ClickButtonCommon();

public:
    void ShowNotification(const std::string msgId, const std::string &text, bool useTwocase);

protected:
    HICON m_hIcon;

    // 생성된 메시지 맵 함수
    virtual BOOL OnInitDialog();
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg BOOL PreTranslateMessage(MSG* pMsg);
    HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
    afx_msg void OnClickedOK();
    afx_msg void OnClickedYes();
    afx_msg void OnClickedNo();
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    DECLARE_MESSAGE_MAP()

private:
    DECLARE_SIGNAL(OnClickOK, bool)
    DECLARE_SIGNAL(OnClickYes, bool)
    DECLARE_SIGNAL(OnClickNo, bool)
    DECLARE_SIGNAL(OnResponse, std::string, std::string)
};

#endif

