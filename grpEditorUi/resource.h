//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// grpEditorUi.rc에서 사용되고 있습니다.
//
#define IDD_FORMVIEW                    101
#define IDD_MULTI_GRP_EDITOR            101
#define IDD_GRPEDITORUI_DIALOG          102
#define IDR_MAINFRAME                   132
#define IDD_NOTIFY_MSG_BOX              133
#define IDD_SEL_FILEDLG                 136
#define SELFILE_DLG_BITMAP1             139
#define SELFILE_DLG_BITMAP2             140
#define IDC_MFCBUTTON1                  1001
#define SUBPAGE_ADD                     1001
#define IDC_MFCBUTTON2                  1002
#define EDITOR_OPEN                     1002
#define SUBPAGE_DEL                     1002
#define EDITOR_FILENAME                 1003
#define SUBPAGE_SAVE                    1003
#define NOTIFYBOX_YES                   1004
#define EDITOR_OUTPUT                   1004
#define SUBPAGE_SELECT                  1004
#define NOTIFYBOX_NO                    1005
#define EDITOR_INC_XOFF                 1005
#define NOTIFYBOX_OK                    1006
#define EDITOR_DEC_YOFF                 1006
#define NOTIFYBOX_TEXT                  1007
#define EDITOR_INC_YOFF                 1007
#define SELFILE_DLG_LIST                1008
#define EDITOR_SHOW_XYDOT               1008
#define SELFILE_DLG_OK                  1009
#define SELFILE_DLG_UP                  1010
#define SELFILE_DLG_SHOWPATH            1011
#define SELFILE_DLG_CLOSE               1012
#define EDITOR_PARAM_WIDTH              1016
#define EDITOR_PARAM_HEIGHT             1017
#define EDITOR_PARAM_XOFF               1018
#define EDITOR_PARAM_YOFF               1019
#define EDITOR_IMG_VIEW                 1020
#define EDITOR_WIDTH                    1021
#define EDITOR_HEIGHT                   1022
#define EDITOR_XOFF                     1023
#define EDITOR_YOFF                     1024
#define EDITOR_DISP_BORDER              1025
#define EDITOR_VIEW_PANEL               1026
#define EDITOR_FINFO_PANEL              1027
#define EDITOR_CTL_PANEL                1028
#define EDITOR_VIEW_BASE                1029
#define EDITOR_DEC_XOFF                 1031
#define EDITOR_XY_POINT                 1032
#define EDITOR_MULTI_PANEL              1033
#define SUBPAGE_LIST                    1034
#define IDC_BUTTON1                     1035
#define IDC_BUTTON2                     1036
#define IDC_BUTTON3                     1037
#define IDC_BUTTON4                     1038

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        144
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1036
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
