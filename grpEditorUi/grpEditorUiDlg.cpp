
// grpEditorUiDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "grpEditorUi.h"
#include "grpEditorUiDlg.h"
#include "afxdialogex.h"
#include "coreui.h"

#include "notifyboxdlg.h"
#include "selectfiledlg.h"
#include "pagemanager.h"
#include "subpage.h"
#include "eventworker.h"
#include "stringhelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "GrpLib")

using namespace _StringHelper;

// CgrpEditorUiDlg 대화 상자

CgrpEditorUiDlg::CgrpEditorUiDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_GRPEDITORUI_DIALOG, pParent), m_bkBrush(new CBrush(RGB(63, 72, 204)))
{
    m_viewRect = std::make_unique<CRect>();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_coreui = std::make_unique<CoreUi>(this);
}

CgrpEditorUiDlg::~CgrpEditorUiDlg()
{ }

void CgrpEditorUiDlg::InitCControls()
{
    m_filenameDisplay.SetWindowText("ready...");
    m_viewbase.SetWindowTextA("");

    m_filenameDisplay.SetTextColor(RGB(181, 230, 29));

    m_btnOpenFile.ModifyWndName("Fetch");
    m_btnOutput.ModifyWndName("Output");

    m_textParamWidth.SetWindowTextA("width:");
    m_textParamHeight.SetWindowTextA("height:");
    m_textParamXOff.SetWindowTextA("xoffset:");
    m_textParamYOff.SetWindowTextA("yoffset:");
    m_textParamWidth.SetTextColor(RGB(192, 32, 225));
    m_textParamHeight.SetTextColor(RGB(192, 32, 225));
    m_textParamXOff.SetTextColor(RGB(192, 32, 225));
    m_textParamYOff.SetTextColor(RGB(192, 32, 225));

    m_dispWidth.SetTextColor(RGB(12, 243, 215));
    m_dispHeight.SetTextColor(RGB(12, 243, 215));
    m_dispXOff.SetTextColor(RGB(12, 243, 215));
    m_dispYOff.SetTextColor(RGB(12, 243, 215));

    m_imgview.GetWindowRect(m_viewRect.get());

    m_btnDecXOff.ModifyWndName("-");
    m_btnDecYOff.ModifyWndName("-");
    m_btnIncXOff.ModifyWndName("+");
    m_btnIncYOff.ModifyWndName("+");
    m_btnDec10XOff.ModifyWndName("-10");
    m_btnDec10YOff.ModifyWndName("-10");
    m_btnInc10XOff.ModifyWndName("+10");
    m_btnInc10YOff.ModifyWndName("+10");
}

void CgrpEditorUiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
    auto controlExchange = [pDX](int nIDC, CWnd &wnd)
    { DDX_Control(pDX, nIDC, wnd); };

    controlExchange(EDITOR_OPEN, m_btnOpenFile);
    controlExchange(EDITOR_OUTPUT, m_btnOutput);
    controlExchange(EDITOR_FILENAME, m_filenameDisplay);
    controlExchange(EDITOR_PARAM_HEIGHT, m_textParamHeight);
    controlExchange(EDITOR_PARAM_WIDTH, m_textParamWidth);
    controlExchange(EDITOR_PARAM_XOFF, m_textParamXOff);
    controlExchange(EDITOR_PARAM_YOFF, m_textParamYOff);
    controlExchange(EDITOR_IMG_VIEW, m_imgview);
    controlExchange(EDITOR_WIDTH, m_dispWidth);
    controlExchange(EDITOR_HEIGHT, m_dispHeight);
    controlExchange(EDITOR_XOFF, m_dispXOff);
    controlExchange(EDITOR_YOFF, m_dispYOff);
    controlExchange(EDITOR_DISP_BORDER, m_dispPanel);
    controlExchange(EDITOR_VIEW_PANEL, m_viewPanel);
    controlExchange(EDITOR_FINFO_PANEL, m_finfoPanel);
    controlExchange(EDITOR_CTL_PANEL, m_ctlPanel);
    controlExchange(EDITOR_VIEW_BASE, m_viewbase);
    controlExchange(EDITOR_DEC_XOFF, m_btnDecXOff);
    controlExchange(EDITOR_INC_XOFF, m_btnIncXOff);
    controlExchange(EDITOR_DEC_YOFF, m_btnDecYOff);
    controlExchange(EDITOR_INC_YOFF, m_btnIncYOff);
    
    controlExchange(IDC_BUTTON1, m_btnDec10XOff);
    controlExchange(IDC_BUTTON2, m_btnInc10XOff);
    controlExchange(IDC_BUTTON3, m_btnDec10YOff);
    controlExchange(IDC_BUTTON4, m_btnInc10YOff);
    controlExchange(EDITOR_XY_POINT, m_xypointDisp);
    m_xypointDisp.SetBackgroundColor(RGB(0, 255, 0));

    controlExchange(EDITOR_SHOW_XYDOT, m_btnShowXyToggle);
    InitCControls();
    m_btnShowXyToggle.ModifyWndName("Appear/Hidden Adjust mark");
}

void CgrpEditorUiDlg::ControlsRegistCallback()
{
    m_btnOpenFile.SetCallback([this]() { this->OnClickedOpenFile(); });
    m_btnDecXOff.SetCallback([this]() { this->OnClickedChangeXYOffset("xoff", -1); });
    m_btnDecYOff.SetCallback([this]() { this->OnClickedChangeXYOffset("yoff", -1); });
    m_btnIncXOff.SetCallback([this]() { this->OnClickedChangeXYOffset("xoff", 1); });
    m_btnIncYOff.SetCallback([this]() { this->OnClickedChangeXYOffset("yoff", 1); });
    m_btnDec10XOff.SetCallback([this]() { this->OnClickedChangeXYOffset("xoff", -10); });
    m_btnDec10YOff.SetCallback([this]() { this->OnClickedChangeXYOffset("yoff", -10); });
    m_btnInc10XOff.SetCallback([this]() { this->OnClickedChangeXYOffset("xoff", 10); });
    m_btnInc10YOff.SetCallback([this]() { this->OnClickedChangeXYOffset("yoff", 10); });

    m_btnOutput.SetCallback([this]() { this->OnClickedOutput(); });
    m_btnShowXyToggle.SetCallback([this]() { this->OnClickedToggleXYDotVisible(); });
}

void CgrpEditorUiDlg::Initialize()
{
    EventWorker &worker = EventWorker::Instance();

    worker.Start();

    m_mainimg = std::make_unique<CImage>();
    m_notifyboxWnd = std::shared_ptr<NotifyBoxDlg>(new NotifyBoxDlg(this), [](NotifyBoxDlg *wnd) { wnd->DestroyWindow(); delete wnd; });
    m_notifyboxWnd->Create(IDD_NOTIFY_MSG_BOX, this);

    m_coreui->SetNotifyBox(m_notifyboxWnd);

    m_filelistWnd = std::unique_ptr<SelectFileDlg, std::function<void(SelectFileDlg*)>>(new SelectFileDlg(this), [](SelectFileDlg *wnd) { wnd->DestroyWindow(); delete wnd; });
    m_filelistWnd->Create(IDD_SEL_FILEDLG, this);

    m_filelistWnd->OnSelectedFile().Connection(&CoreUi::OnFileCheckout, m_coreui.get());

    m_coreui->OnUpdateXYOffset().Connection(&CoreUi::InvokeUpdateXYDisp, m_coreui.get());

    UpdateImageView({ }, { });

    worker.AppendTask(&m_coreui->OnUpdateXYOffset(), 0, 0);

    InitPageManager();
    ControlsRegistCallback();
    SetWindowText("GRP EDITOR GUI -- Released. October 14 2023 Happy soft LTD");
}

void CgrpEditorUiDlg::InitPageManager()
{
    CWnd *loaderFrame = GetDlgItem(EDITOR_MULTI_PANEL);
    m_pageman = PageManager::MakeInstance(*loaderFrame, this);

    SubPage *subpage = new SubPage(IDD_MULTI_GRP_EDITOR, this);

    m_coreui->OnSendSubPageUpdate().Connection(&SubPage::EventReceiver::DoUpdateSubPageList, subpage->Receiver().get());
    subpage->OnButtonClicked().Connection(&CoreUi::ListenSubPage, m_coreui.get());

    m_pageman->MakePage("subpage", std::unique_ptr<CWnd>(subpage));
    m_pageman->ShowPage("subpage");

    loaderFrame->SetWindowTextA("");
}

void CgrpEditorUiDlg::DrawImageViewBorder()
{
    CClientDC cdc(this);
    CPen pen(PS_SOLID, 1, RGB(255, 0, 0));
    CPen *oldpen = cdc.SelectObject(&pen);

    cdc.Rectangle(m_viewRect.get());
    cdc.SelectObject(oldpen);
}

BEGIN_MESSAGE_MAP(CgrpEditorUiDlg, CDialogEx)
    ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CgrpEditorUiDlg 메시지 처리기

BOOL CgrpEditorUiDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

    Initialize();

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CgrpEditorUiDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

        //DrawImageViewBorder();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CgrpEditorUiDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CgrpEditorUiDlg::OnClose()
{
    m_notifyboxWnd.reset();

    EventWorker &worker = EventWorker::Instance();

    worker.Stop();
    if (m_pageman)
        m_pageman.reset();
    CDialogEx::OnClose();
}

BOOL CgrpEditorUiDlg::PreTranslateMessage(MSG* pMsg)
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_RETURN:
        case VK_ESCAPE:
        case VK_F1:
            return TRUE;
        }
    }
    return CDialogEx::PreTranslateMessage(pMsg);
}

HBRUSH CgrpEditorUiDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
    HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

    if (pWnd==this)
        return *m_bkBrush;

    return hbr;
}

void CgrpEditorUiDlg::OnClickedOpenFile()
{
    m_filelistWnd->ShowWnd();
}

void CgrpEditorUiDlg::OnClickedOutput()
{
    if (!m_mainimg->IsNull())
       m_notifyboxWnd->ShowNotification("saveformat", "would you want saving a format as grp?", true);
}

void CgrpEditorUiDlg::OnClickedChangeXYOffset(const char *offset, int n)
{
    m_coreui->ReportChangeOffset(offset, [n](uint32_t val) { return val + n; });
}

//void CgrpEditorUiDlg::OnClickedIncreaseXOff(int n)
//{
//    m_coreui->ReportChangeOffset("xoff", [](uint32_t val) { return val + 1; });
//}
//
//void CgrpEditorUiDlg::OnClickedDecreaseYOff(int n)
//{
//    m_coreui->ReportChangeOffset("yoff", [](uint32_t val) { return val - 1; });
//}
//
//void CgrpEditorUiDlg::OnClickedIncreaseYOff(int n)
//{
//    m_coreui->ReportChangeOffset("yoff", [](uint32_t val) { return val + 1; });
//}

void CgrpEditorUiDlg::OnClickedToggleXYDotVisible()
{
    static UINT colors[] = { 0x80ff00, 0xffff80, 0x80ffff, 0xff80ff, 0xffffff };
    static int colrIndex = 1;
    bool isvisible = m_xypointDisp.IsWindowVisible() & true;

    m_xypointDisp.ShowWindow(isvisible ? SW_HIDE : SW_SHOW);
    if (!isvisible)
        m_xypointDisp.SetBackgroundColor(colors[(++colrIndex) % (sizeof(colors)/sizeof(UINT))]);
}

void CgrpEditorUiDlg::UpdateCurrentFileDisplayer(const std::string &file)
{
    m_filenameDisplay.SetWindowTextA(file.c_str());
}

void CgrpEditorUiDlg::UpdateImageView(const std::string & imgurl, const CRect &rect, bool textonly)
{
    bool clear = imgurl.empty();

    if (!textonly)
    {
        if (!m_mainimg->IsNull())
            m_mainimg->Detach();

        if (!clear)
            m_mainimg->Load(imgurl.c_str());

        m_imgview.SetBitmap(*m_mainimg);
        m_imgview.Invalidate();
    }
    m_dispXOff.SetWindowText(clear ? "" : toArray(stringFormat("%d", rect.left)));
    m_dispYOff.SetWindowText(clear ? "" : toArray(stringFormat("%d", rect.top)));
    m_dispWidth.SetWindowText(clear ? "" : toArray(stringFormat("%d", rect.right)));
    m_dispHeight.SetWindowText(clear ? "" : toArray(stringFormat("%d", rect.bottom)));
}

void CgrpEditorUiDlg::TeleportXYDisplay(int xpos, int ypos)
{
    CRect viewRect;

    m_viewbase.GetWindowRect(&viewRect);
    ScreenToClient(&viewRect);

    m_xypointDisp.MoveWindow(viewRect.left + xpos, viewRect.top + ypos, 3, 3);
    m_xypointDisp.Invalidate();
}


