
#include "stdafx.h"
#include "selectfiledlg.h"
#include "notifyboxdlg.h"
#include "resource.h"
#include "stringhelper.h"

#include "eventworker.h"

#include <functional>
#include <filesystem>

#define VISUAL_STUDIO_2015 1900
#if _MSC_VER == VISUAL_STUDIO_2015
#define NAMESPACE_FILESYSTEM std::experimental::filesystem
#else
#define NAMESPACE_FILESYSTEM std::filesystem
#endif
#undef VISUAL_STUDIO_2015

#include <array>

using namespace _StringHelper;

SelectFileDlg::SelectFileDlg(CWnd *parent)
    : CDialogEx()
{
    m_parent = parent;
    m_currentPath = GetDefaultPath();
    m_imgList = std::make_unique<CImageList>();
}

void SelectFileDlg::SetControlsCallback()
{
    m_btnOk.SetCallback([this]() { this->OnClickedChoose(); });
    m_btnClose.SetCallback([this]() { this->OnClickedClose(); });
    m_moveToUp.SetCallback([this]() { this->OnClickedMoveToUp(); });
}

void SelectFileDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);

    DDX_Control(pDX, SELFILE_DLG_OK, m_btnOk);
    DDX_Control(pDX, SELFILE_DLG_CLOSE, m_btnClose);
    DDX_Control(pDX, SELFILE_DLG_LIST, m_listview);
    DDX_Control(pDX, SELFILE_DLG_SHOWPATH, m_displaypath);
    DDX_Control(pDX, SELFILE_DLG_UP, m_moveToUp);

    m_listview.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
    m_btnOk.ModifyWndName("Choose this");
    m_btnClose.ModifyWndName("Close");
    m_moveToUp.ModifyWndName("Go to parent");
    SetControlsCallback();
}

std::string SelectFileDlg::GetDefaultPath()
{
    std::array<char, 0x200> getpath;

    getpath.fill(0);
    ::GetModuleFileName(nullptr, getpath.data(), getpath.size());

    std::string fullpath(getpath.data());
    auto findoff = fullpath.find_last_of('\\');

    return fullpath.substr(0, findoff);
}

void SelectFileDlg::InitializeControls()
{
    m_imgList->Create(17, 20, ILC_COLOR8, 3, 3);

    using cbitmap_ptr = std::unique_ptr<CBitmap, std::function<void(CBitmap*)>>;
    cbitmap_ptr imgDir = cbitmap_ptr(new CBitmap, [](CBitmap *bit) { bit->DeleteObject(); delete bit; });
    cbitmap_ptr imgFile = cbitmap_ptr(new CBitmap, [](CBitmap *bit) { bit->DeleteObject(); delete bit; });
    UINT transparentColor = RGB(255, 0, 255);

    imgFile->LoadBitmapA(SELFILE_DLG_BITMAP1);
    imgDir->LoadBitmapA(SELFILE_DLG_BITMAP2);
    
    m_imgList->Add(imgFile.get(), transparentColor);
    m_imgList->Add(imgDir.get(), transparentColor);
    
    CRect listctrlRect;

    m_listview.GetWindowRect(&listctrlRect);
    m_listview.InsertColumn(file_list_column_img, "image", LVCFMT_LEFT, 30);
    m_listview.InsertColumn(file_list_column_name, "path", LVCFMT_LEFT, listctrlRect.Width());
    m_listview.SetImageList(m_imgList.get(), LVSIL_SMALL);

    UpdateFilelist();
}

void SelectFileDlg::UpdateFilelist()
{
    uint32_t itemcount = 0;
    auto getFileNameOnly = [](const std::string &path)
    {
        auto findoff = path.find_last_of('\\');

        return path.substr(findoff == std::string::npos ? 0 : findoff + 1);
    };

    m_listview.SetRedraw(false);
    m_listview.DeleteAllItems();

    for (const auto &entry : NAMESPACE_FILESYSTEM::directory_iterator(m_currentPath))
    {
        m_listview.InsertItem(itemcount, "", NAMESPACE_FILESYSTEM::is_directory(entry.path()) ? 1 : 0);
        m_listview.SetItemText(itemcount, file_list_column_name, getFileNameOnly(entry.path().string()).c_str());

        ++itemcount;
    }
    m_displaypath.SetWindowTextA(m_currentPath.c_str());
    m_listview.SetRedraw(true);
}

void SelectFileDlg::GoDirectory(const std::string &dir)
{
    m_currentPath = stringFormat("%s\\%s", m_currentPath, dir);
    UpdateFilelist();
}

void SelectFileDlg::SetNotifyBox(const std::shared_ptr<NotifyBoxDlg>& notifybox)
{
    m_notifybox = notifybox;
}

void SelectFileDlg::ShowWnd()
{
    if (m_parent != nullptr)
        m_parent->ShowWindow(SW_HIDE);

    m_selectedItemName = "";
    ShowWindow(SW_SHOW);
}

void SelectFileDlg::CloseWnd()
{
    if (m_parent != nullptr)
        m_parent->ShowWindow(SW_SHOW);
    ShowWindow(SW_HIDE);
}

BOOL SelectFileDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
    //  프레임워크가 이 작업을 자동으로 수행합니다.
    SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
    SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

                                    // TODO: 여기에 추가 초기화 작업을 추가합니다.
    InitializeControls();
    CenterWindow();

    return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

BEGIN_MESSAGE_MAP(SelectFileDlg, CDialogEx)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_NOTIFY(LVN_ITEMCHANGED, SELFILE_DLG_LIST, OnChangedSelecting)
END_MESSAGE_MAP()

void SelectFileDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 아이콘을 그립니다.
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

HCURSOR SelectFileDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

BOOL SelectFileDlg::PreTranslateMessage(MSG* pMsg)
{
    // TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
    if (pMsg->message == WM_KEYDOWN)
    {
        switch (pMsg->wParam)
        {
        case VK_RETURN:
        case VK_ESCAPE:
        case VK_F1:
            return TRUE;
        }
    }
    return CDialogEx::PreTranslateMessage(pMsg);
}

void SelectFileDlg::OnChangedSelecting(NMHDR *pNMHDR, LRESULT *pResult)
{
    NM_LISTVIEW *pNMListview = reinterpret_cast<NM_LISTVIEW*>(pNMHDR);

    if ((pNMListview->uChanged & LVIF_STATE) && (pNMListview->uNewState & LVIS_SELECTED))
    {
        m_selectedItemName = m_listview.GetItemText(pNMListview->iItem, file_list_column_name);
    }
}

void SelectFileDlg::OnClickedChoose()
{
    if (m_selectedItemName.empty())
        return;

    if (m_selectedItemName.find_last_of('.') == std::string::npos)
        GoDirectory(m_selectedItemName);
    else
    {
        EventWorker &worker = EventWorker::Instance();

        worker.AppendTask(&m_OnSelectedFile, stringFormat("%s\\%s", m_currentPath, m_selectedItemName));

        CloseWnd();
    }
}

void SelectFileDlg::OnClickedMoveToUp()
{
    auto findoff = m_currentPath.find_last_of('\\');

    if (findoff == std::string::npos)
        return;

    m_currentPath = m_currentPath.substr(0, findoff);
    UpdateFilelist();
}

void SelectFileDlg::OnClickedClose()
{
    CloseWnd();
}
