
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include "eventsignalslot.h"
#include "subpageitem.h"
#include <map>

class CgrpEditorUiDlg;
class NotifyBoxDlg;
class ImgFormat;
class MultipleGrp;
class PageManager;

class CoreUi : public CCObject
{
    enum class FFormat
    {
        NONE,
        BMP,
        GRP,
        PNG,
        MGRP
    };
private:
    CgrpEditorUiDlg *m_mainwnd;
    std::weak_ptr<NotifyBoxDlg> m_notifybox;
    std::shared_ptr<ImgFormat> m_targetFormat;
    std::map<std::string, FFormat> m_extensionMap;
    FFormat m_openFormat;
    std::unique_ptr<MultipleGrp> m_grpPack;
    std::weak_ptr<PageManager> m_pageman;

public:
    CoreUi(CgrpEditorUiDlg *mainwnd);
    ~CoreUi() override;

private:
    bool BmpLoadProcess(const std::string &bmpfile, CRect &destRect);
    bool GrpLoadProcess(const std::string &grpfile, CRect &destRect);
    bool MultiGrpLoadProcess(const std::string &mgrpfile);
    bool CheckFormat(const std::string &extension, FFormat &result);

public:
    void OnFileCheckout(const std::string &file);
    void SetNotifyBox(const std::shared_ptr<NotifyBoxDlg> &notifybox);
    void SetPageManager(const std::shared_ptr<PageManager> &pageman)
    {
        m_pageman = pageman;
    }

    void PutOutput();
    void ReportChangeOffset(const std::string &offsetName, std::function<uint32_t(uint32_t)> &&operation);
    void InvokeUpdateXYDisp(int x, int y);

private:
    bool AddFormat();
    bool DeleteFormat(int delkey);
    bool ReloadFormat(int selKey);
    bool UpdateSubpageList();

public:
    void ListenSubPage(const std::string &msg, int key);

private:
    void NotifyResponseResult(const std::string msgid, const std::string result);

private:
    DECLARE_SIGNAL(OnUpdateXYOffset, int, int)
    DECLARE_SIGNAL(OnSendSubPageUpdate, std::list<SubPageListItem>)
};

#endif

