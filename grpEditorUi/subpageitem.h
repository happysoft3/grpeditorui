
#ifndef SUB_PAGE_ITEM_H__
#define SUB_PAGE_ITEM_H__

#include <string>

struct SubPageListItem
{
    std::string m_content;
    int m_int;

    static constexpr int content_index = 0;
    static constexpr int int_index = 1;
};

#endif

